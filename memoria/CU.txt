CU-0
[Usuario]-(Nuestras pizzas)
[Usuario]-(Crea tu pizza)
[Usuario]-(Ver pedido)
[Usuario]-(Salir)

CU-1
[Usuario]-(Añadir pizza)
[Usuario]-(Ver pedido)
[Usuario]-(Volver al inicio)
[Usuario]-(Volver atrás)

CU-2
[Usuario]-(Elegir masa)
[Usuario]-(Elegir salsa)
[Usuario]-(Incrementar ingrediente)
[Usuario]-(Reducir ingrediente)
(Añadir al pedido)-[Usuario]
(Ver pedido)-[Usuario]
(Volver al inicio)-[Usuario]
(Volver atrás)-[Usuario]

CU-3
[Usuario]-(Eliminar pizza)
[Usuario]-(Cambiar cantidad)
[Usuario]-(Editar pizza)
[Usuario]-(Confirmar pedido)
[Usuario]-(Volver al inicio)
[Usuario]-(Volver atrás)

CU-3.4
[Usuario]-(Completar pedido)
[Usuario]-(Ver pedido)
[Usuario]-(Volver al inicio)
[Usuario]-(Volver atrás)

