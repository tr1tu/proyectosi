/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

/**
 *
 * @author victor
 */
public class Tests {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Dog d = new Dog();
        d.setName("test");
        
        DogContainer c = new DogContainer();
        c.setD(d);
        
        System.out.println(c.getD().getName());
        
        d.setName("changed");
        
        System.out.println(c.getD().getName());
    }
    
}
