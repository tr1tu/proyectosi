/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pizzas;

/**
 *
 * @author victor
 */
public class Pizza {


    public Pizza()
    {
        _name = "";
        _id = -1;
        _doughType = 0;
        _doughThickness = 0;
        _olives = 0;
        _tuna = 0;
        _bacon = 0;
        _onion = 0;
        _caramelizedOnion = 0;
        _mushroom = 0;
        _york = 0;
        _ham = 0;
        _greenPepper = 0;
        _pineapple = 0;
        _chicken = 0;
        _veal = 0;
        _tomato = 0;
        _price = 0;
        _maxIngredientUnits = 99;
        
    }
    
    /**
     * @return the _name
     */
    public String getName() {
        return _name;
    }

    /**
     * @param _name the _name to set
     */
    public void setName(String _name) {
        this._name = _name;
    }
    
    /**
     * @return the _id
     */
    public int getId() {
        return _id;
    }

    /**
     * @param _id the _id to set
     */
    public void setId(int _id) {
        this._id = _id;
    }

    
    /**
     * @return the _doughType
     */
    public int getDoughType() {
        return _doughType;
    }

    /**
     * @param _doughType the _doughType to set
     */
    public void setDoughType(int _doughType) {
        if(_doughType < 0 || _doughType > 2)
            this._doughType = 0;
        else
            this._doughType = _doughType;
    }

    /**
     * @return the _doughThickness
     */
    public int getDoughThickness() {
        return _doughThickness;
    }

    /**
     * @param _doughThickness the _doughThickness to set
     */
    public void setDoughThickness(int _doughThickness) {
        this._doughThickness = _doughThickness;
    }

    /**
     * @return the _olives
     */
    public int getOlives() {
        return _olives;
    }

    /**
     * @param _olives the _olives to set
     */
    public void setOlives(int _olives) {
        this._olives = ingredientCheckedValue(_olives);
    }

    /**
     * @return the _tuna
     */
    public int getTuna() {
        return _tuna;
    }

    /**
     * @param _tuna the _tuna to set
     */
    public void setTuna(int _tuna) {
        this._tuna = ingredientCheckedValue(_tuna);
    }

    /**
     * @return the _bacon
     */
    public int getBacon() {
        return _bacon;
    }

    /**
     * @param _bacon the _bacon to set
     */
    public void setBacon(int _bacon) {
        this._bacon = ingredientCheckedValue(_bacon);
    }

    /**
     * @return the _onion
     */
    public int getOnion() {
        return _onion;
    }

    /**
     * @param _onion the _onion to set
     */
    public void setOnion(int _onion) {
        this._onion = ingredientCheckedValue(_onion);
    }

    /**
     * @return the _caramelizedOnion
     */
    public int getCaramelizedOnion() {
        return _caramelizedOnion;
    }

    /**
     * @param _caramelizedOnion the _caramelizedOnion to set
     */
    public void setCaramelizedOnion(int _caramelizedOnion) {
        this._caramelizedOnion = ingredientCheckedValue(_caramelizedOnion);
    }

    /**
     * @return the _mushroom
     */
    public int getMushroom() {
        return _mushroom;
    }

    /**
     * @param _mushroom the _mushroom to set
     */
    public void setMushroom(int _mushroom) {
        this._mushroom = ingredientCheckedValue(_mushroom);
    }

    /**
     * @return the _york
     */
    public int getYork() {
        return _york;
    }

    /**
     * @param _york the _york to set
     */
    public void setYork(int _york) {
        this._york = ingredientCheckedValue(_york);
    }

    /**
     * @return the _ham
     */
    public int getHam() {
        return _ham;
    }

    /**
     * @param _ham the _ham to set
     */
    public void setHam(int _ham) {
        this._ham = ingredientCheckedValue(_ham);
    }

    /**
     * @return the _greenPepper
     */
    public int getGreenPepper() {
        return _greenPepper;
    }

    /**
     * @param _greenPepper the _greenPepper to set
     */
    public void setGreenPepper(int _greenPepper) {
        this._greenPepper = ingredientCheckedValue(_greenPepper);
    }

    /**
     * @return the _pineapple
     */
    public int getPineapple() {
        return _pineapple;
    }

    /**
     * @param _pineapple the _pineapple to set
     */
    public void setPineapple(int _pineapple) {
        this._pineapple = ingredientCheckedValue(_pineapple);
    }

    /**
     * @return the _chicken
     */
    public int getChicken() {
        return _chicken;
    }

    /**
     * @param _chicken the _chicken to set
     */
    public void setChicken(int _chicken) {
        this._chicken = ingredientCheckedValue(_chicken);
    }

    /**
     * @return the _veal
     */
    public int getVeal() {
        return _veal;
    }

    /**
     * @param _veal the _veal to set
     */
    public void setVeal(int _veal) {
        this._veal = ingredientCheckedValue(_veal);
    }

    /**
     * @return the _tomato
     */
    public int getTomato() {
        return _tomato;
    }

    /**
     * @param _tomato the _tomato to set
     */
    public void setTomato(int _tomato) {
        this._tomato = ingredientCheckedValue(_tomato);
    }
    
    public int getNumberOfIngredients()
    {
        return _olives + _tuna + _bacon + _onion + _caramelizedOnion +
                _mushroom + _york + _ham + _greenPepper + _pineapple +
                _chicken + _veal + _tomato;
    }
    
    public double getPrice()
    {
        if(_price > 0)
            return _price;
        else
        {
            double price = 0;
            
            if(_doughType == 0)
                price = 5;
            else
                price = 7;
            
            price += getNumberOfIngredients() * 1.5;
            
            return price;
        }
    }
    
    public void setPrice(double price)
    {
        _price = price;
    }

    /**
     * @return the _maxIngredientUnits
     */
    public int getMaxIngredientUnits() {
        return _maxIngredientUnits;
    }

    /**
     * @param _maxIngredientUnits the _maxIngredientUnits to set
     */
    public void setMaxIngredientUnits(int _maxIngredientUnits) {
        this._maxIngredientUnits = _maxIngredientUnits;
    }
    
    private int ingredientCheckedValue(int value)
    {
        if(value < 0)
            return 0;
        else if(value > _maxIngredientUnits)
            return _maxIngredientUnits;
        else
            return value;
    }
    
    
    private String _name;
    private int _id;
    private int _doughType;
    private int _doughThickness;
    private int _olives;
    private int _tuna;
    private int _bacon;
    private int _onion;
    private int _caramelizedOnion;
    private int _mushroom;
    private int _york;
    private int _ham;
    private int _greenPepper;
    private int _pineapple;
    private int _chicken;
    private int _veal;
    private int _tomato;
    private double _price; //Para pizzas predefinidas.
    private int _maxIngredientUnits;

    
}
