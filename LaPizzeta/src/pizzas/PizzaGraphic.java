/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pizzas;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;
import javax.imageio.ImageIO;

/**
 *
 * @author victor
 */
public class PizzaGraphic extends javax.swing.JPanel {

    private Pizza _pizza;
    
    private int _imagesPerUnit;
    private long _firstSeed;
    
    private int _ingredientWidth;
    private int _ingredientHeight;
    private BufferedImage _imgDough;
    private BufferedImage _imgDoughTomato;
    private BufferedImage _imgDoughBarbecue;
    private BufferedImage _imgOlives;
    private BufferedImage _imgTuna;
    private BufferedImage _imgBacon;
    private BufferedImage _imgOnion;
    private BufferedImage _imgCaramelizedOnion;
    private BufferedImage _imgMushroom;
    private BufferedImage _imgYork;
    private BufferedImage _imgHam;
    private BufferedImage _imgGreenPepper;
    private BufferedImage _imgPineapple;
    private BufferedImage _imgChicken;
    private BufferedImage _imgVeal;
    private BufferedImage _imgTomato;
    private BufferedImage _imgMozzarella;

    /**
     * @return the _pizza
     */
    public Pizza getPizza() {
        return _pizza;
    }

    /**
     * @param _pizza the _pizza to set
     */
    public void setPizza(Pizza _pizza) {
        this._pizza = _pizza;
    }
    
    /**
     * @return the _imagesPerUnit
     */
    public int getImagesPerUnit() {
        return _imagesPerUnit;
    }

    /**
     * @param _imagesPerUnit the _imagesPerUnit to set
     */
    public void setImagesPerUnit(int _imagesPerUnit) {
        this._imagesPerUnit = _imagesPerUnit;
    }

    /**
     * @return the _ingredientWidth
     */
    public int getIngredientWidth() {
        return _ingredientWidth;
    }

    /**
     * @param _ingredientWidth the _ingredientWidth to set
     */
    public void setIngredientWidth(int _ingredientWidth) {
        this._ingredientWidth = _ingredientWidth;
    }

    /**
     * @return the _ingredientHeight
     */
    public int getIngredientHeight() {
        return _ingredientHeight;
    }

    /**
     * @param _ingredientHeight the _ingredientHeight to set
     */
    public void setIngredientHeight(int _ingredientHeight) {
        this._ingredientHeight = _ingredientHeight;
    }
    
    /**
     * Creates new form PizzaGraphic
     */
    public PizzaGraphic() {
        loadImages();
        generateFirstSeed();
        _imagesPerUnit = 7; 
        _ingredientWidth = 28;
        _ingredientHeight = 28;
        initComponents();
       _pizza = new Pizza();
    }
    
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponents(g);
        
        if(getPizza() != null)
        {
            paintDough(g);
            paintIngredient(g, _imgTomato, getPizza().getTomato(), _firstSeed+12);
            paintIngredient(g, _imgOlives, getPizza().getOlives(), _firstSeed);
            paintIngredient(g, _imgTuna, getPizza().getTuna(), _firstSeed+1);
            paintIngredient(g, _imgBacon, getPizza().getBacon(), _firstSeed+2);
            paintIngredient(g, _imgOnion, getPizza().getOnion(), _firstSeed+3);
            paintIngredient(g, _imgCaramelizedOnion, getPizza().getCaramelizedOnion(), _firstSeed+4);
            paintIngredient(g, _imgMushroom, getPizza().getMushroom(), _firstSeed+5);
            paintIngredient(g, _imgYork, getPizza().getYork(), _firstSeed+6);
            paintIngredient(g, _imgHam, getPizza().getHam(), _firstSeed+7);
            paintIngredient(g, _imgGreenPepper, getPizza().getGreenPepper(), _firstSeed+8);
            paintIngredient(g, _imgPineapple, getPizza().getPineapple(), _firstSeed+9);
            paintIngredient(g, _imgChicken, getPizza().getChicken(), _firstSeed+10);
            paintIngredient(g, _imgVeal, getPizza().getVeal(), _firstSeed+11);
            paintMozzarella(g);
            
            
        }

    }
    
    private void paintDough(Graphics g)
    {
        switch(getPizza().getDoughType())
        {
            case 0:
                g.drawImage(_imgDough, 0, 0, getWidth(), getHeight(), this);
                break;
            case 1:
                g.drawImage(_imgDoughTomato, 0, 0, getWidth(), getHeight(), this);
                break;
            case 2:
                g.drawImage(_imgDoughBarbecue, 0, 0, getWidth(), getHeight(), this);
                break;
            default:
                g.drawImage(_imgDough, 0, 0, getWidth(), getHeight(), this);
        }
    }
    
    private void paintIngredient(Graphics g, BufferedImage img, int quantity, long seed)
    {
        //Obtenemos el punto central del círculo sobre la que se dibujará.
        Point center = new Point(getWidth()/2, getHeight()/2);
        //Se obtiene el mayor radio permitido.
        int maxRadius = getWidth()/2 - Integer.max((int)Math.round(_ingredientHeight*0.54),
                                                   (int)Math.round(_ingredientWidth*0.54));
        //Generador de números aleatorios.
        Random generator = new Random(seed);
        
        for(int i = 0; i < getImagesPerUnit() * quantity; i++)
        {
            //Obtenemos un punto aleatorio en una circunferencia de radio 1.
            double t = 2 * Math.PI * generator.nextDouble();
            double u = generator.nextDouble() + generator.nextDouble();
            double r;

            if(u > 1)
                r = 2 - u;
            else
                r = u;
            
            double x = r * Math.cos(t);
            double y = r * Math.sin(t);
            
            //Pasamos el punto obtenido a la escala deseada.
            int scaledX = (int)Math.round(maxRadius * x);
            int scaledY = (int)Math.round(maxRadius * y);

            //Dibujamos la imagen.
            g.drawImage(img, center.x + scaledX - getIngredientWidth() / 2,
                    center.y + scaledY - getIngredientHeight() / 2, getIngredientWidth(),
                    getIngredientHeight(), this);
        }
    }
    
    private void paintMozzarella(Graphics g)
    {
        if(_pizza.getNumberOfIngredients() > 0)
        {
            g.drawImage(_imgMozzarella, 5, 5, getWidth() - 5, getHeight() - 5, this);
        }
    }
    
    private void loadImages()
    {     
        try
        {         
            InputStream is = getClass().getResourceAsStream("/icons/dough.png");
            _imgDough = ImageIO.read(is);

            is = getClass().getResourceAsStream("/icons/dough.png");
            _imgDough = ImageIO.read(is);

            is = getClass().getResourceAsStream("/icons/dough_tomato.png");
            _imgDoughTomato = ImageIO.read(is);

            is = getClass().getResourceAsStream("/icons/dough_bbq.png");
            _imgDoughBarbecue = ImageIO.read(is);

            is = getClass().getResourceAsStream("/icons/olives_thumb.png");
            _imgOlives = ImageIO.read(is);

            is = getClass().getResourceAsStream("/icons/tuna_thumb.png");
            _imgTuna = ImageIO.read(is);

            is = getClass().getResourceAsStream("/icons/bacon_thumb.png");
            _imgBacon = ImageIO.read(is);

            is = getClass().getResourceAsStream("/icons/onion_thumb.png");
            _imgOnion = ImageIO.read(is);

            is = getClass().getResourceAsStream("/icons/caramelized_onion_thumb.png");
            _imgCaramelizedOnion = ImageIO.read(is);

            is = getClass().getResourceAsStream("/icons/mushroom_thumb.png");
            _imgMushroom = ImageIO.read(is);

            is = getClass().getResourceAsStream("/icons/york_thumb.png");
            _imgYork = ImageIO.read(is);

            is = getClass().getResourceAsStream("/icons/ham_thumb.png");
            _imgHam = ImageIO.read(is);

            is = getClass().getResourceAsStream("/icons/green_pepper_thumb.png");
            _imgGreenPepper = ImageIO.read(is);

            is = getClass().getResourceAsStream("/icons/pineapple_thumb.png");
            _imgPineapple = ImageIO.read(is);

            is = getClass().getResourceAsStream("/icons/chicken_thumb.png");
            _imgChicken = ImageIO.read(is);

            is = getClass().getResourceAsStream("/icons/veal_thumb.png");
            _imgVeal = ImageIO.read(is);

            is = getClass().getResourceAsStream("/icons/tomato_thumb.png");
            _imgTomato = ImageIO.read(is);
            
            is = getClass().getResourceAsStream("/icons/mozzarella.png");
            _imgMozzarella = ImageIO.read(is);
        }
        catch(IOException e)
        {
            System.out.println("ERROR: Could not get some images. " + e.getMessage());
        }
    }
    
    private void generateFirstSeed()
    {
        Random r = new Random();
        _firstSeed = r.nextLong();
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
