/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapizzeta;

import java.util.ArrayList;
import java.util.List;
import pizzas.Pizza;

/**
 *
 * @author victor
 */
public class Order {
    
    private final ArrayList<Pizza> _pizzas;
    private final ArrayList<Integer> _quantities;
    
    private final List<OrderListener> listeners = new ArrayList<>();
    
    public Order()
    {
        _pizzas = new ArrayList<>();
        _quantities = new ArrayList<>();
    }
    
    public Pizza getPizza(int i)
    {
        return _pizzas.get(i);
    }
    
    public Pizza getPizzaById(int id)
    {
        for(Pizza p : _pizzas)
        {
            if(p.getId() == id)
                return p;
        }
        
        Pizza p = new Pizza();
        return p;
    }
    
    public int getQuantity(int i)
    {
        return _quantities.get(i);
    }
    
    public int getQuantityById(int id)
    {
        for(int i = 0; i < _pizzas.size(); i++)
        {
            if(_pizzas.get(i).getId() == id)
                return _quantities.get(i);
        }
        return -1;
    }
    
    public void addPizza(Pizza p, int quantity)
    {
        if(quantity > 0)
        {
            for(int i = 0; i < _pizzas.size(); i++)
            {
                if(_pizzas.get(i).getId() == p.getId())
                {
                    _quantities.set(i, quantity + _quantities.get(i));
                    notifyChange();
                    return;
                }
            }
        
        
            _pizzas.add(p);
            _quantities.add(quantity);
            notifyChange();
        }
    }
    
    public void addEditPizza(Pizza p, int quantity)
    {
        if(quantity > 0)
        {
            for(int i = 0; i < _pizzas.size(); i++)
            {
                if(_pizzas.get(i).getId() == p.getId())
                {
                    _quantities.set(i, quantity);
                    notifyChange();
                    return;
                }
            }
        
        
            _pizzas.add(p);
            _quantities.add(quantity);
            notifyChange();
        }
    }
    
    public boolean deletePizzaByIndex(int i)
    {
        if(_pizzas.size() > i)
        {
            _pizzas.remove(i);
            _quantities.remove(i);
            notifyChange();
            return true;
        }
        return false;
    }
    
    public boolean deletePizzaById(int id)
    {
        for(int i = 0; i < _pizzas.size(); i++)
        {
            if(_pizzas.get(i).getId() == id)
            {
                _pizzas.remove(i);
                _quantities.remove(i);
                notifyChange();
                return true;
            }
        }
        return false;
    }
    
    public int getPizzaCount()
    {
        return _pizzas.size();
    }
    
    public void addListener(OrderListener toAdd)
    {
        listeners.add(toAdd);
    }
    
    public void notifyChange()
    {
        listeners.forEach((ol) -> {
            ol.orderChanged();
        });
    }
    
    public double getPrice()
    {
        double price = 0;
        for(int i = 0; i < _pizzas.size(); i++)
        {
            price += _pizzas.get(i).getPrice() * _quantities.get(i);
        }
        
        return price;
    }
    
    public void clear()
    {
        _pizzas.clear();
        _quantities.clear();
        
        notifyChange();
    }
    
}
