/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapizzeta;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.swing.JOptionPane;
import pizzas.Pizza;

/**
 *
 * @author victor
 */
public class OurPizzas extends javax.swing.JInternalFrame implements Localizable {

    private static OurPizzas _instance = null;
    
    
    public static OurPizzas getInstance()
    {
        if(_instance == null)
            _instance = new OurPizzas();
        return _instance;
    }
    
    private Order getOrder()
    {
        return ((MainFrame)getDesktopPane().getRootPane().getParent()).getOrder();
    }
    
    /**
     * Creates new form OurPizzas
     */
    public OurPizzas() {
        initComponents();
        
        this.getContentPane().setBackground(new java.awt.Color(255, 228, 166));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel4 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        lblClassic = new javax.swing.JLabel();
        txtClassic = new javax.swing.JTextField();
        btnClassic = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        lblBolognese = new javax.swing.JLabel();
        txtBolognese = new javax.swing.JTextField();
        btnBolognese = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        lblBarbecue = new javax.swing.JLabel();
        txtBarbecue = new javax.swing.JTextField();
        btnBarbecue = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        lblHam = new javax.swing.JLabel();
        txtHam = new javax.swing.JTextField();
        btnHam = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        lblHawaiian = new javax.swing.JLabel();
        txtHawaiian = new javax.swing.JTextField();
        btnHawaiian = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        lblCarbonara = new javax.swing.JLabel();
        txtCarbonara = new javax.swing.JTextField();
        btnCarbonara = new javax.swing.JButton();

        setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        jScrollPane2.setBackground(new java.awt.Color(255, 228, 166));
        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane2.setOpaque(false);

        jPanel4.setBackground(new java.awt.Color(255, 228, 166));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel1.setBackground(new java.awt.Color(255, 228, 166));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setPreferredSize(new java.awt.Dimension(200, 245));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/pizza_classic.png"))); // NOI18N
        jLabel1.setPreferredSize(new java.awt.Dimension(200, 183));

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("lapizzeta/Bundle"); // NOI18N
        lblClassic.setText(bundle.getString("OurPizzas.lblClassic.text")); // NOI18N

        txtClassic.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtClassic.setText(bundle.getString("OurPizzas.txtClassic.text")); // NOI18N

        btnClassic.setBackground(new java.awt.Color(87, 204, 73));
        btnClassic.setText(bundle.getString("OurPizzas.btnClassic.text")); // NOI18N
        btnClassic.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClassicActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(txtClassic, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnClassic, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(80, 80, 80)
                .addComponent(lblClassic)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblClassic)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtClassic, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClassic)))
        );

        jPanel2.setBackground(new java.awt.Color(255, 228, 166));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setPreferredSize(new java.awt.Dimension(200, 245));

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/pizza_bolognese.png"))); // NOI18N
        jLabel3.setPreferredSize(new java.awt.Dimension(200, 183));

        lblBolognese.setText(bundle.getString("OurPizzas.lblBolognese.text")); // NOI18N

        txtBolognese.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtBolognese.setText(bundle.getString("OurPizzas.txtBolognese.text")); // NOI18N

        btnBolognese.setBackground(new java.awt.Color(87, 204, 73));
        btnBolognese.setText(bundle.getString("OurPizzas.btnBolognese.text")); // NOI18N
        btnBolognese.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBologneseActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(txtBolognese, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnBolognese, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(77, 77, 77)
                .addComponent(lblBolognese)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblBolognese)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBolognese, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBolognese)))
        );

        jPanel3.setBackground(new java.awt.Color(255, 228, 166));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setPreferredSize(new java.awt.Dimension(200, 245));

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/pizza_bbq.png"))); // NOI18N
        jLabel5.setPreferredSize(new java.awt.Dimension(200, 183));

        lblBarbecue.setText(bundle.getString("OurPizzas.lblBarbecue.text")); // NOI18N

        txtBarbecue.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtBarbecue.setText(bundle.getString("OurPizzas.txtBarbecue.text")); // NOI18N

        btnBarbecue.setBackground(new java.awt.Color(87, 204, 73));
        btnBarbecue.setText(bundle.getString("OurPizzas.btnBarbecue.text")); // NOI18N
        btnBarbecue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBarbecueActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(txtBarbecue, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnBarbecue, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(71, 71, 71)
                .addComponent(lblBarbecue)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblBarbecue)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBarbecue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBarbecue)))
        );

        jPanel6.setBackground(new java.awt.Color(255, 228, 166));
        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel6.setPreferredSize(new java.awt.Dimension(200, 245));

        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/pizza_ham.png"))); // NOI18N
        jLabel9.setPreferredSize(new java.awt.Dimension(200, 183));

        lblHam.setText(bundle.getString("OurPizzas.lblHam.text")); // NOI18N

        txtHam.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtHam.setText(bundle.getString("OurPizzas.txtHam.text")); // NOI18N

        btnHam.setBackground(new java.awt.Color(87, 204, 73));
        btnHam.setText(bundle.getString("OurPizzas.btnHam.text")); // NOI18N
        btnHam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHamActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(txtHam, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnHam, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(83, 83, 83)
                .addComponent(lblHam)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblHam)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtHam, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnHam)))
        );

        jPanel7.setBackground(new java.awt.Color(255, 228, 166));
        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel7.setPreferredSize(new java.awt.Dimension(200, 245));

        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/pizza_hawaiian.png"))); // NOI18N
        jLabel11.setPreferredSize(new java.awt.Dimension(200, 183));

        lblHawaiian.setText(bundle.getString("OurPizzas.lblHawaiian.text")); // NOI18N

        txtHawaiian.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtHawaiian.setText(bundle.getString("OurPizzas.txtHawaiian.text")); // NOI18N

        btnHawaiian.setBackground(new java.awt.Color(87, 204, 73));
        btnHawaiian.setText(bundle.getString("OurPizzas.btnHawaiian.text")); // NOI18N
        btnHawaiian.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHawaiianActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(txtHawaiian, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnHawaiian, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(74, 74, 74)
                .addComponent(lblHawaiian)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblHawaiian)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtHawaiian, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnHawaiian)))
        );

        jPanel5.setBackground(new java.awt.Color(255, 228, 166));
        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel5.setPreferredSize(new java.awt.Dimension(200, 245));

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/pizza_carbonara.png"))); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(200, 183));

        lblCarbonara.setText(bundle.getString("OurPizzas.lblCarbonara.text")); // NOI18N

        txtCarbonara.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtCarbonara.setText(bundle.getString("OurPizzas.txtCarbonara.text")); // NOI18N

        btnCarbonara.setBackground(new java.awt.Color(87, 204, 73));
        btnCarbonara.setText(bundle.getString("OurPizzas.btnCarbonara.text")); // NOI18N
        btnCarbonara.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCarbonaraActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(txtCarbonara, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCarbonara, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(68, 68, 68)
                .addComponent(lblCarbonara)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblCarbonara)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCarbonara, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCarbonara)))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(66, 66, 66)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(66, 66, 66)
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(66, 66, 66)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(66, 66, 66)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(57, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jScrollPane2.setViewportView(jPanel4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnClassicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClassicActionPerformed
        Pizza p = new Pizza();
        p.setName("classic");
        p.setPrice(10.5);
        p.setId(1);
        getOrder().addPizza(p, Integer.parseInt(txtClassic.getText()));
        
        ResourceBundle rb = ResourceBundle.getBundle("lapizzeta/Bundle", Locale.getDefault());
        String s = MessageFormat.format(
                rb.getString("OurPizzas.addClassic"),
                txtClassic.getText(),
                getOrder().getQuantityById(1));
        JOptionPane.showMessageDialog(null, s, "", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_btnClassicActionPerformed

    private void btnBologneseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBologneseActionPerformed
        Pizza p = new Pizza();
        p.setName("bolognese");
        p.setPrice(11.5);
        p.setId(2);
        getOrder().addPizza(p, Integer.parseInt(txtBolognese.getText()));
        
        ResourceBundle rb = ResourceBundle.getBundle("lapizzeta/Bundle", Locale.getDefault());
        String s = MessageFormat.format(
                rb.getString("OurPizzas.addBolognese"),
                txtBolognese.getText(),
                getOrder().getQuantityById(2));
        JOptionPane.showMessageDialog(null, s, "", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_btnBologneseActionPerformed

    private void btnBarbecueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBarbecueActionPerformed
        Pizza p = new Pizza();
        p.setName("barbecue");
        p.setPrice(11.5);
        p.setId(3);
        getOrder().addPizza(p, Integer.parseInt(txtBarbecue.getText()));
        
        ResourceBundle rb = ResourceBundle.getBundle("lapizzeta/Bundle", Locale.getDefault());
        String s = MessageFormat.format(
                rb.getString("OurPizzas.addBarbecue"),
                txtBarbecue.getText(),
                getOrder().getQuantityById(3));
        JOptionPane.showMessageDialog(null, s, "", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_btnBarbecueActionPerformed

    private void btnCarbonaraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCarbonaraActionPerformed
        Pizza p = new Pizza();
        p.setName("carbonara");
        p.setPrice(11.5);
        p.setId(4);
        getOrder().addPizza(p, Integer.parseInt(txtCarbonara.getText()));
        
        ResourceBundle rb = ResourceBundle.getBundle("lapizzeta/Bundle", Locale.getDefault());
        String s = MessageFormat.format(
                rb.getString("OurPizzas.addCarbonara"),
                txtCarbonara.getText(),
                getOrder().getQuantityById(4));
        JOptionPane.showMessageDialog(null, s, "", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_btnCarbonaraActionPerformed

    private void btnHamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHamActionPerformed
        Pizza p = new Pizza();
        p.setName("ham");
        p.setPrice(10.5);
        p.setId(5);
        getOrder().addPizza(p, Integer.parseInt(txtHam.getText()));
        
        ResourceBundle rb = ResourceBundle.getBundle("lapizzeta/Bundle", Locale.getDefault());
        String s = MessageFormat.format(
                rb.getString("OurPizzas.addHam"),
                txtHam.getText(),
                getOrder().getQuantityById(5));
        JOptionPane.showMessageDialog(null, s, "", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_btnHamActionPerformed

    private void btnHawaiianActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHawaiianActionPerformed
        Pizza p = new Pizza();
        p.setName("hawaiian");
        p.setPrice(11.5);
        p.setId(6);
        getOrder().addPizza(p, Integer.parseInt(txtHawaiian.getText()));
        
        ResourceBundle rb = ResourceBundle.getBundle("lapizzeta/Bundle", Locale.getDefault());
        String s = MessageFormat.format(
                rb.getString("OurPizzas.addHawaiian"),
                txtHawaiian.getText(),
                getOrder().getQuantityById(6));
        JOptionPane.showMessageDialog(null, s, "", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_btnHawaiianActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBarbecue;
    private javax.swing.JButton btnBolognese;
    private javax.swing.JButton btnCarbonara;
    private javax.swing.JButton btnClassic;
    private javax.swing.JButton btnHam;
    private javax.swing.JButton btnHawaiian;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblBarbecue;
    private javax.swing.JLabel lblBolognese;
    private javax.swing.JLabel lblCarbonara;
    private javax.swing.JLabel lblClassic;
    private javax.swing.JLabel lblHam;
    private javax.swing.JLabel lblHawaiian;
    private javax.swing.JTextField txtBarbecue;
    private javax.swing.JTextField txtBolognese;
    private javax.swing.JTextField txtCarbonara;
    private javax.swing.JTextField txtClassic;
    private javax.swing.JTextField txtHam;
    private javax.swing.JTextField txtHawaiian;
    // End of variables declaration//GEN-END:variables

    @Override
    public void onLocaleChanged(Locale l) {
        ResourceBundle rb = ResourceBundle.getBundle("lapizzeta/Bundle", l);
        
        lblBarbecue.setText(rb.getString("OurPizzas.lblBarbecue.text"));
        lblBolognese.setText(rb.getString("OurPizzas.lblBolognese.text"));
        lblCarbonara.setText(rb.getString("OurPizzas.lblCarbonara.text"));
        lblClassic.setText(rb.getString("OurPizzas.lblClassic.text"));
        lblHam.setText(rb.getString("OurPizzas.lblHam.text"));
        lblHawaiian.setText(rb.getString("OurPizzas.lblHawaiian.text"));
        txtBarbecue.setText(rb.getString("OurPizzas.txtBarbecue.text"));
        txtBolognese.setText(rb.getString("OurPizzas.txtBolognese.text"));
        txtCarbonara.setText(rb.getString("OurPizzas.txtCarbonara.text"));
        txtClassic.setText(rb.getString("OurPizzas.txtClassic.text"));
        txtHam.setText(rb.getString("OurPizzas.txtHam.text"));
        txtHawaiian.setText(rb.getString("OurPizzas.txtHawaiian.text"));
        btnBarbecue.setText(rb.getString("OurPizzas.btnBarbecue.text"));
        btnBolognese.setText(rb.getString("OurPizzas.btnBolognese.text"));
        btnCarbonara.setText(rb.getString("OurPizzas.btnCarbonara.text"));
        btnClassic.setText(rb.getString("OurPizzas.btnClassic.text"));
        btnHam.setText(rb.getString("OurPizzas.btnHam.text"));
        btnHawaiian.setText(rb.getString("OurPizzas.btnHawaiian.text"));
    }
}
