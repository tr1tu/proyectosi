/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapizzeta;

import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.layout.Border;
import javafx.scene.paint.Color;
import javax.swing.ImageIcon;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import pizzas.Pizza;

/**
 *
 * @author victor
 */
public class MainFrame extends javax.swing.JFrame implements OrderListener {

    private JInternalFrame _currentFrame;
    private JInternalFrame _lastFrame;
    
    private final Order _order;
    private static MainFrame _instance = null;
    
    private final ArrayList<Localizable> _localizableItems;
    
    private Queue<Locale> _locales;
    
    
    public static MainFrame getInstance()
    {
        if(_instance == null)
            _instance = new MainFrame();
        return _instance;
    }
    
    /**
     * Creates new form MainFrame
     */
    private MainFrame() {
        
        _order = new Order();
        _order.addListener(this);
        _order.addListener(MyOrder.getInstance());
        _localizableItems = new ArrayList<>();
        _locales = new LinkedList<>();
        
        //Añadimos los diferentes idiomas.
        _locales.add(new Locale("en", "GB"));
        _locales.add(new Locale("es", "ES"));
        
        
        initComponents();
        //Creamos los diferentes JInternalFrames
        createFrame(Home.getInstance());
        createFrame(MyOrder.getInstance());
        createFrame(CreatePizza.getInstance());
        createFrame(OurPizzas.getInstance());
        createFrame(ConfirmOrder.getInstance());
        
        listenHome();
        
        //Mostramos el frame de inicio.
        _lastFrame = Home.getInstance();
        showFrame(Home.getInstance());
        
        updateOrderText();
        
        this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/logo.png")));
        
        this.getContentPane().setBackground(new java.awt.Color(242, 191, 109));
        
        
    }
    
    private void createFrame(JInternalFrame frame)
    {
        //Quitamos la barra de titulo.
        ((BasicInternalFrameUI)frame.getUI()).setNorthPane(null);
        //Añadimos el frame al DesktopPane.
        desktop.add(frame);
        
        try
        {
            frame.setMaximum(true);
        }
        catch(PropertyVetoException e)
        {
            System.err.println("createFrame error");
        }
        
        if(frame instanceof Localizable)
            _localizableItems.add((Localizable) frame);

    }
    
    public final void showFrame(JInternalFrame frame)
    {
        for(JInternalFrame f : desktop.getAllFrames())
        {
            f.setVisible(false);
        }
        _lastFrame = _currentFrame;
        _currentFrame = frame;
        
        frame.setVisible(true);
    }
    
    private void showLastFrame()
    {    
        showFrame(_lastFrame);   
    }

    private void listenHome()
    {
        JLabel lblCreate, lblPizzas;
        lblCreate = Home.getInstance().getCreateLabel();
        lblPizzas = Home.getInstance().getPizzasLabel();
        
        lblCreate.addMouseListener(new java.awt.event.MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                CreatePizza.getInstance().setPizza(new Pizza());
                showFrame(CreatePizza.getInstance());
            }

            @Override
            public void mousePressed(MouseEvent e) {}

            @Override
            public void mouseReleased(MouseEvent e) {}

            @Override
            public void mouseEntered(MouseEvent e) {}

            @Override
            public void mouseExited(MouseEvent e) {}
        });
        
        lblPizzas.addMouseListener(new java.awt.event.MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                showFrame(OurPizzas.getInstance());
            }

            @Override
            public void mousePressed(MouseEvent e) {}

            @Override
            public void mouseReleased(MouseEvent e) {}

            @Override
            public void mouseEntered(MouseEvent e) {}

            @Override
            public void mouseExited(MouseEvent e) {}
        });
    }
    
    public Order getOrder()
    {
        return _order;
    }
    
    public void changeLocale(Locale l)
    {
        Locale.setDefault(l);
        //Cambiar el idioma de este frame.
        updateOrderText();
        
        //Cambiar el idioma de todos los JFrame.
        _localizableItems.forEach((lif) -> {
            lif.onLocaleChanged(l);
        });
        
        //Actualizar icono del botón.
        if(Locale.getDefault().toLanguageTag().equals("en-GB"))
        {          
            lblLocation.setIcon(new ImageIcon(getClass().getResource("/icons/spanish.png")));
        }
        else
        {
            lblLocation.setIcon(new ImageIcon(getClass().getResource("/icons/english.png")));
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        desktop = new javax.swing.JDesktopPane();
        btnHome = new javax.swing.JButton();
        btnOrder = new javax.swing.JButton();
        btnLast = new javax.swing.JButton();
        lblLocation = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("lapizzeta/Bundle"); // NOI18N
        setTitle(bundle.getString("MainFrame.title")); // NOI18N
        setResizable(false);

        btnHome.setBackground(new java.awt.Color(255, 255, 255));
        btnHome.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        btnHome.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/home.png"))); // NOI18N
        btnHome.setToolTipText(bundle.getString("MainFrame.btnHome.toolTipText")); // NOI18N
        btnHome.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 0));
        btnHome.setBorderPainted(false);
        btnHome.setContentAreaFilled(false);
        btnHome.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnHome.setDefaultCapable(false);
        btnHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHomeActionPerformed(evt);
            }
        });

        btnOrder.setBackground(new java.awt.Color(220, 214, 79));
        btnOrder.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        btnOrder.setText("Mi pedido (" + getOrder().getPizzaCount() + ")"
        );
        btnOrder.setToolTipText(bundle.getString("MainFrame.btnOrder.toolTipText")); // NOI18N
        btnOrder.setBorderPainted(false);
        btnOrder.setDefaultCapable(false);
        btnOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOrderActionPerformed(evt);
            }
        });

        btnLast.setBackground(new java.awt.Color(255, 255, 255));
        btnLast.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        btnLast.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/goback.png"))); // NOI18N
        btnLast.setToolTipText(bundle.getString("MainFrame.btnLast.toolTipText")); // NOI18N
        btnLast.setBorderPainted(false);
        btnLast.setContentAreaFilled(false);
        btnLast.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnLast.setDefaultCapable(false);
        btnLast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLastActionPerformed(evt);
            }
        });

        lblLocation.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/english.png"))); // NOI18N
        lblLocation.setText(bundle.getString("MainFrame.lblLocation.text")); // NOI18N
        lblLocation.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblLocation.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblLocationMouseClicked(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("URW Chancery L", 1, 24)); // NOI18N
        jLabel1.setText(bundle.getString("MainFrame.jLabel1.text")); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnLast, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnHome, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 245, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(253, 253, 253)
                .addComponent(lblLocation, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnOrder))
            .addComponent(desktop)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnHome, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnLast, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblLocation, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0)
                .addComponent(desktop, javax.swing.GroupLayout.DEFAULT_SIZE, 518, Short.MAX_VALUE))
        );

        btnLast.getAccessibleContext().setAccessibleName(bundle.getString("MainFrame.btnLast.AccessibleContext.accessibleName")); // NOI18N

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHomeActionPerformed
        showFrame(Home.getInstance());
    }//GEN-LAST:event_btnHomeActionPerformed

    private void btnOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOrderActionPerformed
        showFrame(MyOrder.getInstance());
    }//GEN-LAST:event_btnOrderActionPerformed

    private void btnLastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLastActionPerformed
        showLastFrame();
    }//GEN-LAST:event_btnLastActionPerformed

    private void lblLocationMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblLocationMouseClicked
        Locale l = _locales.poll();
        _locales.add(l);
        changeLocale(l);
    }//GEN-LAST:event_lblLocationMouseClicked
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new MainFrame().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnHome;
    private javax.swing.JButton btnLast;
    private javax.swing.JButton btnOrder;
    private javax.swing.JDesktopPane desktop;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lblLocation;
    // End of variables declaration//GEN-END:variables


    @Override
    public void orderChanged() {
        updateOrderText();
    }
    
    private void updateOrderText()
    {
        ResourceBundle rb = ResourceBundle.getBundle("lapizzeta/Bundle", Locale.getDefault());
        String text = rb.getString("MainFrame.MyOrder");
        btnOrder.setText(text + " (" + getOrder().getPizzaCount() + ")");
    }
}
